# Templates
This repo contains all template files I use. Most of them are for luaLaTeX.

## File descriptions
- *chemicalMacros.tex:* The file I include if I have to write chemical reports and need chemical packages and macros. 
- *lstsetup.tex:* The file I include if I have to include code snippets. It contains layout options and includecode commands.
- *preamble.tex:* This is my normal luaLaTeX preamble. It loads all packages I need, defines new macros, chages certain layout options and presets title and author. 
- *preambleGerman.tex:* This is basically the same as preamble.tex, but is set up for german documents (date, "Abbildung" instead of "Figure", etc.)
- *uncrustify:* Code beautifier, set up for C++
